# Summary
- This repository contains the Jupyter notebook I used to create the maps in this [Medium post](https://medium.com/@eejj/seven-maps-to-better-understand-infrastructure-damage-in-syria-cdbdac427858)
- For questions contact: *anajjar@protonmail.com*
